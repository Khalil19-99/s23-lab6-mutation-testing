from calculator import count
import random


def test_valid_sum():
    a = random.randint(0, 100000)
    b = random.randint(0, 100000)
    assert count(a, b, '+') == (a + b)
    assert count(b, a, '+') == (b + a)


def test_valid_sum_float():
    a = 2.5
    b = 11.32
    assert count(a, b, '+') == (2.5 + 11.32)
    assert count(b, a, '+') == (11.32 + 2.5)


def test_valid_sum_negative_test():
    a = 2.5
    b = -11.32
    assert count(a, b, '+') == (2.5 + (-11.32))
    assert count(b, a, '+') == (-11.32 + 2.5)


def test_valid_sub():
    a = random.randint(0, 100000)
    b = random.randint(0, 100000)
    assert count(a, b, '-') == (a - b)
    assert count(b, a, '-') == (b - a)


def test_valid_sub_float():
    a = 7.3
    b = 2.1
    assert count(a, b, '-') == (7.3 - 2.1)
    assert count(b, a, '-') == (2.1 - 7.3)


def test_valid_sub_negative_float():
    a = 7.3
    b = 11.1
    assert count(a, b, '-') == (7.3 - 11.1)
    assert count(b, a, '-') == (11.1 - 7.3)


def test_valid_div():
    a = random.randint(0, 100000)
    b = random.randint(0, 100000)
    assert count(a, b, '/') == (a / b)
    assert count(b, a, '/') == (b / a)


def test_valid_div_float():
    a = 22
    b = 33
    assert count(a, b, '/') == (22 / 33)
    assert count(b, a, '/') == (33 / 22)


def test_valid_div_negative_float():
    a = -22
    b = 33
    assert count(a, b, '/') == (-22 / 33)
    assert count(b, a, '/') == (-33 / 22)


def test_valid_mult():
    a = random.randint(0, 100000)
    b = random.randint(0, 100000)
    assert count(a, b, '*') == (a * b)
    assert count(b, a, '*') == (b * a)


def test_valid_mult_float():
    a = 0.5
    b = 0.7
    assert count(a, b, '*') == (0.5 * 0.7)
    assert count(b, a, '*') == (0.7 * 0.5)


def test_valid_mult_negative_float():
    a = -0.5
    b = 0.7
    assert count(a, b, '*') == (-0.5 * 0.7)
    assert count(b, a, '*') == (0.7 * (-0.5))


def test_div_by_zero():
    a = random.randint(0, 100000)
    b = 0
    assert count(a, b, '/') == "Division by zero!"
    assert count(b, a, '/') == 0


def test_invalid_operators():
    ctr = 0

    operators = ['(', '0']

    for op in operators:
        try:
            count(random.randint(0, 100000), random.randint(0, 100000), op)
        except UnboundLocalError:
            ctr += 1

    if ctr != len(operators):
        raise ValueError('Value of `ctr` expected to be 1')

